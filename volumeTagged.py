import boto3

client = boto3.client('ec2', aws_access_key_id='x',
                      aws_secret_access_key='y')
volumes = client.describe_volumes()

for vol in volumes['Volumes']:
    if vol['Attachments']:
        _volume_id = vol['Attachments'][0]['VolumeId']
        _instance_id = vol['Attachments'][0]['InstanceId']
        ec2 = client.describe_instances(InstanceIds=[_instance_id])
        name = ec2['Reservations'][0]['Instances'][0]['KeyName']
        if "ecs" in name:
            print(f"{_instance_id}    {name}")
            print(f"Tagging the volume: {_volume_id}")
            response = client.create_tags(
 #               DryRun=True | False,
                Resources=[
                    _volume_id,
                ],
                Tags=[
                    {
                        'Key': 'ServiceName',
                        'Value': 'wordpress ecs platform'
                    },
                    {
                        'Key': 'Environment',
                        'Value': 'dev'
                    },
                    {
                        'Key': 'ServiceCatalogueId',
                        'Value': '471'
                    }
                ]
            )
            result_verify = client.describe_tags(
                Filters=[
                    {
                        'Name': 'resource-id',
                        'Values': [
                            _volume_id,
                        ]
                    },
                ],
            )
            print(F"Verify = {result_verify['Tags']}")
