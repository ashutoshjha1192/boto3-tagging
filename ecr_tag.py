import boto3

client = boto3.client('ecr', aws_access_key_id='x',
                      aws_secret_access_key='y')

response = client.describe_repositories()
for repo in response['repositories']:
    repo_arn = (repo['repositoryArn'])
    tag = client.tag_resource( resourceArn= repo_arn,
          tags=[{
              'Key': 'ServiceCatalogueId',
              'Value': '3'
          },
            {
                        'Key': 'ServiceName',
                        'Value': 'myTest'
                    },
                    {
                        'Key': 'Environment',
                        'Value': 'dev'
                    },]
    )
